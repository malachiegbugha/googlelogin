const express = require('express');
const app = express();
const cookieParser = require("cookie-parser");
//Goofle auth
const {OAuth2Client} = require('google-auth-library');
const CLIENT_ID= '1045668157554-4252r413h2ninrcnjl1l9orsq7c5qfaq.apps.googleusercontent.com';
const client = new OAuth2Client(CLIENT_ID);
const PORT = process.env.PORT || 5000;
//MIddeleware
app.set('view engine', 'ejs');
app.use(express.json());
app.use(cookieParser());
app.get('/',(req,res)=>{
    res.render('index');

});
app.get('/login',(req,res)=>{
    res.render('login');

})
app.post('/login',(req,res)=>{

    let token=req.body.token;
    async function verify(){
        //async function verify(token){
   const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID,
    });
    const payload = ticket.getPayload();
    //const userid = payload['sub'];
    //console.log(payload);
   // if(payload){
     //   return payload;
    //}
    //return null;
}
verify().then(()=>{
    res.cookie('session-token', token);
    res.send('success');
}).catch(console.error);
//const res = await verify(token)
//if(!res){
  //  console.error('Failed log')

//}

})
app.get('/dashboard',checkAuthenticated,(req,res)=>{
    let user = req.user;
    res.render('dashboard',{user})
})
app.get('/protectedroute',checkAuthenticated,(req,res)=>{
    res.render('protectedroute');
})
app.get('logout',(req,res)=>{
    res.clearCookie('session-token');
    res.redirect('/login')
})
function checkAuthenticated(req,res,next){
    let token = req.cookies('session-token');
    let user={};
    async function verify() {
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: CLIENT_ID,  
        });
        const payload = ticket.getPayload();
        user.name = payload.name;
        user.email = payload.email;
        user.picture = payload.picture;
       
  
      }
      verify().then(()=>{
          req.user = user;
          next()
      }).catch(res.redirect('/login'));
}
app.listen(PORT, ()=>{
    console.log(`Server runnning of port ${PORT}`)
})